b = c(5, 11, 18, 21, 29, 40, 48, 48, 57, 72, 80, 76, 69, 87, 94, 85)
x = runif(length(b), 0, 10)  #random initial guess
xtmp = c()

a = function(i, j)
{
	if (i == j)
		return(-4)
	if (abs(i-j) == 1 || abs(i-j) == 4)
		return(1)
	else
		return(0)
}

epsilon=1e-06
max1=10000
for (k in 1:max1)
{
	for (i in 1:length(x))
	{
		sum = 0
		for (j in 1:length(x))
		{
			if (i != j)
				sum = sum + a(i,j) * x[j]
		}
		xtmp[i] = (b[i] - sum)/a(i,i)
	}
	err=abs(max(x-xtmp))
	relerr=err/(abs(max(xtmp)))
	if (err<epsilon) #(relerr<epsilon)
		break
	x = xtmp
}

print(x)
print(k)
